Rust on OpenBSD
===============

# Getting rust-analyzer to work

Clone it.

Build with `cargo xtask install --server`

To run, you need the rust-src component. This can be copied from a Linux system with the **same Rust version** from the path `$(rustc --print sysroot)/lib/rustlib/src/rust/src`.

Point to that directory with the environment variable $RUST_SRC_PATH.

For building code that requires nightly Rust, use this: https://github.com/semarie/build-rust

This may also be useful: https://gitlab.com/AaronM04/dummy_libdl
